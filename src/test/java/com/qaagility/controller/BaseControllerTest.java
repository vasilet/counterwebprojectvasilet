package com.qaagility.controller;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

public class BaseControllerTest extends Mockito {

    @Test
    public void welcomeTest() throws Exception {
		ModelMap mm = mock(ModelMap.class);
		
		BaseController bc = new BaseController();
		String output = bc.welcome(mm);
		assertEquals(output, "index");

    }
	
	@Test
    public void welcomeNameTest() throws Exception {

        ModelMap mm = mock(ModelMap.class);
		
		BaseController bc = new BaseController();
		String output = bc.welcomeName("aaa", mm);
		assertEquals(output, "index");

    }
}

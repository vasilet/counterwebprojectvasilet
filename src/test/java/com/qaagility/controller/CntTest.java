package com.qaagility.controller;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.junit.Test;
import org.mockito.Mockito;

public class CntTest extends Mockito {

    @Test
    public void dRegularDivisionTest() throws Exception {

        int k= new Cnt().d(10,2);
        assertEquals("Regular Division",5,k);

    }
	
	@Test
    public void dZeroDivisionTest() throws Exception {

        int k= new Cnt().d(10, 0);
        assertEquals("Add",Integer.MAX_VALUE,k);

    }


}
